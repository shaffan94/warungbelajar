/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pengenalanpemrograman;

/**
 *
 * @author ASUS
 */
public class ProgramLingkaran {
    public static void main(String[] args) {

        double r, phi, luas, keliling;
        
        r = 7;
        phi = 3.14;
        
        luas = phi*r*r;
        keliling = 2*phi*r;
        
        System.out.print("Luas lingkaran : "+luas);
        System.out.println();
        System.out.print("Keliling lingkaran : "+keliling);
        System.out.println();
    }
}
