/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programtukarnilai;

/**
 *
 * @author Nadiya
 */
public class ProgramTukarNilai {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Deklarasi variabel
    int A;
    int B;
    int temp;

//      Inisiasi Variabel    
    A = 5;
    B = 6;
    
    System.out.println("Sebelum Ditukar");
    System.out.println("A = "+A);
    System.out.println("B = "+B);
    
    temp=A;
    A=B;
    B=temp;
        
        System.out.println("Sesudah Ditukar");
        System.out.println("A = "+A);
        System.out.println("B = "+B);
    
    }
    
}
