/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lingkaran;
import java.util.Scanner;
/**
 *
 * @author Admin
 */
public class Lingkaran {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        float r, luas, keliling;
        float phi;
    
        Scanner s = new Scanner(System.in);
        System.out.print("input jari-jari = "); r=s.nextFloat();
        System.out.print("input phi = "); phi=s.nextFloat();
        
        luas = phi*r*r;
        keliling = 2*phi*r;
        
        System.out.println("Luas Lingkaran= " +luas);
        System.out.println("Keliling Lingkaran= " +keliling);
        
    }
    
}
